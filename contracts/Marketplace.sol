pragma solidity ^0.4.17;

import "./Ownable.sol";
import "./Address.sol";

contract Marketplace{
    
    struct Auction{
        address addr;
        string title;
        string description;
        address[] nodes;
        address owner;
    }
    
    enum NodeType { share_keeper, perturbabion_keeper }
    
    struct Node{
        uint256 ID;                  //Node identifier
        address contract_address;   //Multiple Nodes can share a single contract
        uint8 reputation;
        string phy_address;
        string pub_key;
        NodeType node_type;         //Sybil attack Prevention
    }
    
    address[] public active_auctions;
    address[] public terminated_auctions;
    
    mapping (address => Auction) public auction_list;  //Map that contains both ended and active auction
    uint256 lastID;                                     //Last used ID to assign unique values
    uint256[] active_nodes;
    mapping (uint256 => Node) public nodes_list;         //Map that contains nodes of the network
    
    
    //Event triggered when a node is elected to participate to auction
    event NodeElected(
        uint256 ID,
        address node_address,
        address auction_address
    );
    
    
    mapping (address => uint) deposits;


    constructor(){
        lastID = 0;
    }

    
    function isAuctionActive(address _addr) private view returns(bool){
        uint len = active_auctions.length;
        uint i = 0;
        bool flag = false;
        for(; i<len && !flag; i++){
            if(active_auctions[i] == _addr)
                flag = true;
        }
    
        return flag;
    }
    
    
    
    function electNodes(address auction_address, uint8 nodes_number) public {
        Auction auction = auction_list[auction_address];
        require(auction.owner == msg.sender, "Only the auction owner can elect nodes");
        //Generate random number and add to the array "nodes"" inside the auction_list
        //Then emit the event "NodeElected" to notify the client's node
        emit NodeElected(44, 0xdda0607ea5ea78441aa5650f49f3b8bf6b5079a9, auction_address);
    }
    
    function registerAuction(address _addr, string _title, string _description) public returns(bool){
        
        if(isAuctionActive(_addr))
            return false;           //Auction already listed
            
        if(!Address.isContract(_addr))      
            return false;           //Address does not conrespond to a contract
            
        Auction storage auct;
        auct.addr = _addr;
        auct.title = _title;
        auct.description = _description;
        auct.owner = msg.sender;
        
        active_auctions.push(_addr);
        auction_list[_addr] = auct;
        
        return true;
    }
    
    
    function retrieveAuctionInfo(address _addr) public view returns (string, string){
        Auction storage tmp = auction_list[_addr];
        return (tmp.title, tmp.description);
    }

    function getAuctionAddressFromID(uint auctionID) public view returns(address){
        return active_auctions[auctionID];
    }
    
    
    function getActiveAuctionNumber() public view returns(uint){
        return active_auctions.length;
    }

    function getActiveNodeNumber() public view returns(uint){
        return active_nodes.length;
    }  

    function getNodeDetail(uint256 nodeID) public view returns (address , uint, string){
        return (nodes_list[nodeID].contract_address, nodes_list[nodeID].reputation, nodes_list[nodeID].phy_address);
    }
    
    /**
    *   Function that allow to register a node; a deposit of 1 ETH is required to take part the network
     */
    function registerNode(string _phy_address, string _public_key) public payable returns (uint256){
        
        require(msg.value <= 1, "Send at leat 1 ETH to partecipate the network");
        deposits[msg.sender] += msg.value;

        Node newNode;
        uint256 new_id = lastID + 1;

        active_nodes.push(new_id);

        newNode.ID = new_id;
        newNode.phy_address = _phy_address;
        newNode.pub_key = _public_key;
        newNode.reputation = 10;    //Started value
        newNode.node_type = NodeType.share_keeper;   //Default value

        nodes_list[new_id] = newNode;

        return new_id;
    }

}
