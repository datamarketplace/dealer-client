window.esdocSearchIndex = [
  [
    "react-box/src/auctioncontract.jsx~auctioncontract",
    "class/src/AuctionContract.jsx~AuctionContract.html",
    "<span>AuctionContract</span> <span class=\"search-result-import-path\">react-box/src/AuctionContract.jsx</span>",
    "class"
  ],
  [
    "react-box/src/protocol.jsx~protocol",
    "class/src/Protocol.jsx~Protocol.html",
    "<span>Protocol</span> <span class=\"search-result-import-path\">react-box/src/Protocol.jsx</span>",
    "class"
  ],
  [
    "src/.external-ecmascript.js~array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Array",
    "src/.external-ecmascript.js~Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~arraybuffer",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ArrayBuffer",
    "src/.external-ecmascript.js~ArrayBuffer",
    "external"
  ],
  [
    "src/.external-ecmascript.js~boolean",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean",
    "src/.external-ecmascript.js~Boolean",
    "external"
  ],
  [
    "src/.external-ecmascript.js~dataview",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/DataView",
    "src/.external-ecmascript.js~DataView",
    "external"
  ],
  [
    "src/.external-ecmascript.js~date",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Date",
    "src/.external-ecmascript.js~Date",
    "external"
  ],
  [
    "src/.external-ecmascript.js~error",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Error",
    "src/.external-ecmascript.js~Error",
    "external"
  ],
  [
    "src/.external-ecmascript.js~evalerror",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/EvalError",
    "src/.external-ecmascript.js~EvalError",
    "external"
  ],
  [
    "src/.external-ecmascript.js~float32array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Float32Array",
    "src/.external-ecmascript.js~Float32Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~float64array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Float64Array",
    "src/.external-ecmascript.js~Float64Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~function",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function",
    "src/.external-ecmascript.js~Function",
    "external"
  ],
  [
    "src/.external-ecmascript.js~generator",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Generator",
    "src/.external-ecmascript.js~Generator",
    "external"
  ],
  [
    "src/.external-ecmascript.js~generatorfunction",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/GeneratorFunction",
    "src/.external-ecmascript.js~GeneratorFunction",
    "external"
  ],
  [
    "src/.external-ecmascript.js~infinity",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Infinity",
    "src/.external-ecmascript.js~Infinity",
    "external"
  ],
  [
    "src/.external-ecmascript.js~int16array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Int16Array",
    "src/.external-ecmascript.js~Int16Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~int32array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Int32Array",
    "src/.external-ecmascript.js~Int32Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~int8array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Int8Array",
    "src/.external-ecmascript.js~Int8Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~internalerror",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/InternalError",
    "src/.external-ecmascript.js~InternalError",
    "external"
  ],
  [
    "src/.external-ecmascript.js~json",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/JSON",
    "src/.external-ecmascript.js~JSON",
    "external"
  ],
  [
    "src/.external-ecmascript.js~map",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Map",
    "src/.external-ecmascript.js~Map",
    "external"
  ],
  [
    "src/.external-ecmascript.js~nan",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/NaN",
    "src/.external-ecmascript.js~NaN",
    "external"
  ],
  [
    "src/.external-ecmascript.js~number",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number",
    "src/.external-ecmascript.js~Number",
    "external"
  ],
  [
    "src/.external-ecmascript.js~object",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object",
    "src/.external-ecmascript.js~Object",
    "external"
  ],
  [
    "src/.external-ecmascript.js~promise",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Promise",
    "src/.external-ecmascript.js~Promise",
    "external"
  ],
  [
    "src/.external-ecmascript.js~proxy",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Proxy",
    "src/.external-ecmascript.js~Proxy",
    "external"
  ],
  [
    "src/.external-ecmascript.js~rangeerror",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RangeError",
    "src/.external-ecmascript.js~RangeError",
    "external"
  ],
  [
    "src/.external-ecmascript.js~referenceerror",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/ReferenceError",
    "src/.external-ecmascript.js~ReferenceError",
    "external"
  ],
  [
    "src/.external-ecmascript.js~reflect",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Reflect",
    "src/.external-ecmascript.js~Reflect",
    "external"
  ],
  [
    "src/.external-ecmascript.js~regexp",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/RegExp",
    "src/.external-ecmascript.js~RegExp",
    "external"
  ],
  [
    "src/.external-ecmascript.js~set",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Set",
    "src/.external-ecmascript.js~Set",
    "external"
  ],
  [
    "src/.external-ecmascript.js~string",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String",
    "src/.external-ecmascript.js~String",
    "external"
  ],
  [
    "src/.external-ecmascript.js~symbol",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Symbol",
    "src/.external-ecmascript.js~Symbol",
    "external"
  ],
  [
    "src/.external-ecmascript.js~syntaxerror",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/SyntaxError",
    "src/.external-ecmascript.js~SyntaxError",
    "external"
  ],
  [
    "src/.external-ecmascript.js~typeerror",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/TypeError",
    "src/.external-ecmascript.js~TypeError",
    "external"
  ],
  [
    "src/.external-ecmascript.js~urierror",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/URIError",
    "src/.external-ecmascript.js~URIError",
    "external"
  ],
  [
    "src/.external-ecmascript.js~uint16array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Uint16Array",
    "src/.external-ecmascript.js~Uint16Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~uint32array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Uint32Array",
    "src/.external-ecmascript.js~Uint32Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~uint8array",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Uint8Array",
    "src/.external-ecmascript.js~Uint8Array",
    "external"
  ],
  [
    "src/.external-ecmascript.js~uint8clampedarray",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Uint8ClampedArray",
    "src/.external-ecmascript.js~Uint8ClampedArray",
    "external"
  ],
  [
    "src/.external-ecmascript.js~weakmap",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakMap",
    "src/.external-ecmascript.js~WeakMap",
    "external"
  ],
  [
    "src/.external-ecmascript.js~weakset",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/WeakSet",
    "src/.external-ecmascript.js~WeakSet",
    "external"
  ],
  [
    "src/.external-ecmascript.js~boolean",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Boolean",
    "src/.external-ecmascript.js~boolean",
    "external"
  ],
  [
    "src/.external-ecmascript.js~function",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Function",
    "src/.external-ecmascript.js~function",
    "external"
  ],
  [
    "src/.external-ecmascript.js~null",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/null",
    "src/.external-ecmascript.js~null",
    "external"
  ],
  [
    "src/.external-ecmascript.js~number",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Number",
    "src/.external-ecmascript.js~number",
    "external"
  ],
  [
    "src/.external-ecmascript.js~object",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/Object",
    "src/.external-ecmascript.js~object",
    "external"
  ],
  [
    "src/.external-ecmascript.js~string",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/String",
    "src/.external-ecmascript.js~string",
    "external"
  ],
  [
    "src/.external-ecmascript.js~undefined",
    "https://developer.mozilla.org/en-US/docs/Web/JavaScript/Reference/Global_Objects/undefined",
    "src/.external-ecmascript.js~undefined",
    "external"
  ],
  [
    "src/auctioncontract.jsx",
    "file/src/AuctionContract.jsx.html",
    "src/AuctionContract.jsx",
    "file"
  ],
  [
    "src/auctioncontract.jsx~auctioncontract#callback",
    "class/src/AuctionContract.jsx~AuctionContract.html#instance-member-callback",
    "src/AuctionContract.jsx~AuctionContract#callback",
    "member"
  ],
  [
    "src/auctioncontract.jsx~auctioncontract#connectweb3",
    "class/src/AuctionContract.jsx~AuctionContract.html#instance-method-connectWeb3",
    "src/AuctionContract.jsx~AuctionContract#connectWeb3",
    "method"
  ],
  [
    "src/auctioncontract.jsx~auctioncontract#constructor",
    "class/src/AuctionContract.jsx~AuctionContract.html#instance-constructor-constructor",
    "src/AuctionContract.jsx~AuctionContract#constructor",
    "method"
  ],
  [
    "src/auctioncontract.jsx~auctioncontract#contractinstance",
    "class/src/AuctionContract.jsx~AuctionContract.html#instance-member-contractInstance",
    "src/AuctionContract.jsx~AuctionContract#contractInstance",
    "member"
  ],
  [
    "src/auctioncontract.jsx~auctioncontract#getauctioninfo",
    "class/src/AuctionContract.jsx~AuctionContract.html#instance-method-getAuctionInfo",
    "src/AuctionContract.jsx~AuctionContract#getAuctionInfo",
    "method"
  ],
  [
    "src/auctioncontract.jsx~auctioncontract#instantiatecontract",
    "class/src/AuctionContract.jsx~AuctionContract.html#instance-method-instantiateContract",
    "src/AuctionContract.jsx~AuctionContract#instantiateContract",
    "method"
  ],
  [
    "src/auctioncontract.jsx~auctioncontract#instantiatenodecontract",
    "class/src/AuctionContract.jsx~AuctionContract.html#instance-method-instantiateNodeContract",
    "src/AuctionContract.jsx~AuctionContract#instantiateNodeContract",
    "method"
  ],
  [
    "src/auctioncontract.jsx~auctioncontract#web3",
    "class/src/AuctionContract.jsx~AuctionContract.html#instance-member-web3",
    "src/AuctionContract.jsx~AuctionContract#web3",
    "member"
  ],
  [
    "src/protocol.jsx",
    "file/src/Protocol.jsx.html",
    "src/Protocol.jsx",
    "file"
  ],
  [
    "src/protocol.jsx~protocol#constructor",
    "class/src/Protocol.jsx~Protocol.html#instance-constructor-constructor",
    "src/Protocol.jsx~Protocol#constructor",
    "method"
  ],
  [
    "src/utils/getweb3.js",
    "file/src/utils/getWeb3.js.html",
    "src/utils/getWeb3.js",
    "file"
  ]
]