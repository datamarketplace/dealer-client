# Datamarketplace Client

Client that allow the dealer to interact with the system in order to create and manage an auction.


## Dependencies

*CouchDB* is used as storage layer due its compatibili with android operating system, so it must be present to ensure that system works.

For installation follow the official guide at : http://docs.couchdb.org/en/latest/install/unix.html#installation-using-the-apache-couchdb-convenience-binary-packages 

## Usage

To test the system first you should create a local blockchain with *ganache*: `ganache-cli -i 666`

Then go into the project directory to compile and put the contracts into the blockchain.
````
    truffle compile
    truffle migrate
````
Once contracts are on the blockchain you could start the application with `npm start`.

**NOTE**: The project is still in early phase so it is not tested and it does not even implements all basic functionalities. 



