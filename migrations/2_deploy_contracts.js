var DataMarketplace = artifacts.require("./DataMarketplace.sol");
var Protocol = artifacts.require("./Protocol.sol");
module.exports = function(deployer) {
  deployer.deploy(DataMarketplace);
  deployer.deploy(Protocol);
};
