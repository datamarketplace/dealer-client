import React, {Component} from 'react'
import secret from 'secrets.js-grempe'
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';
import InputLabel from '@material-ui/core/InputLabel';
import FormControl from '@material-ui/core/FormControl';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button';


class SharesManager extends React.Component{


    constructor(props){
        super(props);
        this.state = {
            share_num: 2,
            tres_num: 2,
            password: "",
            dialogOpen: false
        }

        this.handleShareChange = this.handleShareChange.bind(this);
        this.handleTreesholdChange = this.handleTreesholdChange.bind(this);
        this.handleShareCreation = this.handleShareCreation.bind(this);
    }


    handleShareChange(event){
        console.log(event.target.value);
        this.setState({share_num: event.target.value})
    }

    handleTreesholdChange(event){
        console.log(event.target.value);
        this.setState({tres_num: event.target.value})

    }



    handleShareCreation(){

        var hexpass = secret.str2hex(this.state.password);
        console.log("Hexpass: " + hexpass);
        console.log("Creating Share with " + this.state.share_num + " pieces and " + this.state.tres_num + " treeshold")
        var shares = secret.share(hexpass, parseInt(this.state.share_num), parseInt(this.state.tres_num));

        this.props.onComplete(shares);

    }

    handleClickOpen = () => {
        this.setState({ dialogOpen: true });
    };

    handleClose = () => {
        this.setState({ dialogOpen: false });
    };

    render(){

        return(
            <div>
                <div>
                    <Typography variant="body1">Shares Number {this.state.share_num}</Typography>
                    <Typography variant="body1">Threesold Number {this.state.tres_num}</Typography>
                    <Button color="primary" onClick={this.handleClickOpen}>Change</Button>
                    <Button onClick={this.handleShareCreation}>Create Shares</Button>
                    <Dialog
                    disableBackdropClick
                    disableEscapeKeyDown
                    open={this.state.dialogOpen}
                    onClose={this.handleClose}
                    >
                        <DialogTitle>Change shares settings</DialogTitle>
                        <DialogContent>
                            <form>
                                <FormControl>
                                    <InputLabel htmlFor="shares_num">Shares Number</InputLabel>
                                    <input type="number" name="quantity" min="2" max="10" onChange={this.handleShareChange} value={this.state.share_num}></input>
                                </FormControl>
                                <FormControl>
                                    <InputLabel htmlFor="age-simple">Treeshold Number</InputLabel>
                                    <input type="number" name="quantity" min="2" max="5" onChange={this.handleTreesholdChange} value={this.state.tres_num}></input>
                                </FormControl>
                            </form>
                        </DialogContent>
                        <DialogActions>
                            <Button onClick={this.handleClose} color="primary">
                            Confirm
                            </Button>
                        </DialogActions>
                    </Dialog>
                </div>

            </div>
        );
    }

}


export default SharesManager;