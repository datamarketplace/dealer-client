import React, {Component} from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import OfflineIcon from '@material-ui/icons/SignalCellular0BarRounded'
import OnlineIcon from '@material-ui/icons/SignalCellular4BarRounded'
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import Dialog from '@material-ui/core/Dialog';
import DialogActions from '@material-ui/core/DialogActions';
import DialogContent from '@material-ui/core/DialogContent';
import DialogTitle from '@material-ui/core/DialogTitle';

import NodeComunication from './NodeCommunication'

class NodeListElem extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            address: this.props.node.address,
            phy_address: this.props.node.phy_address,
            reputation: this.props.node.reputation,
            communication: null,
            dialogOpen: false,
            isOnline: false
        }

        this.communication = new NodeComunication(this.props.node.phy_address, this.props.node.address, "", "");
        this.checkStatus = this.checkStatus.bind(this);
    }

    handleClickOpen = () => {
        this.setState({ dialogOpen: true });
        this.checkStatus();
    };

    handleClose = () => {
        this.setState({ dialogOpen: false });
    };

    checkStatus() {
        this.communication.isOnline().then( (res) => {
            this.setState({isOnline: true})
        }).catch( (err) => {
            this.setState({isOnline: false})
        })
    };


    render(){

        var icon = "";

        if(this.state.isOnline){
            icon = (<OnlineIcon />)
            console.log("Node IS ONLINE")
        }else{
            icon = (<OfflineIcon />)
        }

        return (
            <div>
                <ListItem button onClick={this.handleClickOpen}>
                    <ListItemIcon>
                        {icon}
                        </ListItemIcon>
                    <ListItemText primary={this.state.address} />
                </ListItem>
                <Dialog
                disableBackdropClick
                disableEscapeKeyDown
                open={this.state.dialogOpen}
                onClose={this.handleClose}
                >
                    <DialogTitle>Node Information</DialogTitle>
                    <DialogContent>
                        <Grid container spacing={8}>
                            <Grid item>
                                <Typography variant="body1">Address: {this.state.address}</Typography>
                            </Grid>
                            <Grid item>
                                <Typography variant="body1">Physical Address: {this.state.phy_address}</Typography>
                            </Grid>
                            <Grid item>
                            {icon}
                            </Grid>
                        </Grid>
                    </DialogContent>
                    <DialogActions>
                        <Button onClick={this.handleClose} color="primary">
                        Ok
                        </Button>
                    </DialogActions>
                </Dialog>
            </div>
        );
    }

}


export default NodeListElem;