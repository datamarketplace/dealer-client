import getWeb3 from './utils/getWeb3';
import ProtocolContract from '../build/contracts/Protocol.json'
import DataMarketplaceContract from '../build/contracts/DataMarketplace.json'


/**
 * Interface for the Auction Contract 
 * @example
 * let contract = new AuctionContract();
 */

class AuctionContract {
        /**
         * Connect to the web3 interface and deploy the contract
         * @param {function} callback is the callback function called after the cotract is
         * instantiated
         */
        constructor(callback, _contract_address=null){
            this.web3 = null;
            this.contractInstance = null;
            this.callback = callback; 
            this.contract_address = _contract_address
            this.connectWeb3();    //Instantiate Web3
        }

        connectWeb3(){
            getWeb3
            .then(results => {
                this.web3 = results.web3
              // Instantiate contract once web3 provided.
            
              this.instantiateContract();

            })
            .catch((error) => {
              console.log('Error finding web3.' + error)
            })
        }

        instantiateContract() {

            const contract = require('truffle-contract')
            const protoContract = contract(ProtocolContract)
            protoContract.setProvider(this.web3.currentProvider)
        
            // Declaring this for later so we can chain functions on protoContract.
            var protoContractInstance

            //this.contract_address = protoContract.options.address;

            // Get accounts.
            this.web3.eth.getAccounts((error, accounts) => {

                if(this.contract_address == null){
                    //If the contract was not selected
                    protoContract.deployed().then((instance) => {
                        protoContractInstance = instance
                        this.contractInstance = protoContractInstance;
                        console.log("AuctionContract - Contract Instantiated");
    
                        this.callback();
    
                    })                   
                }else{
                    protoContract.at(this.contract_address) //Address of the contract, obtained from Etherscan
                    .then(instance =>{
                        protoContractInstance = instance
                        this.contractInstance = protoContractInstance;
                        console.log("AuctionContract - Contract Instantiated from " + this.contract_address);
    
                        this.callback();

                    })
                }

            })
        
        }

        getNodesNumber(){
            return this.contractInstance.getNodesNumber();
        }

        getNodes(){
            return this.getNodesNumber().then(async (count) => {
                console.log("Number Count:" + count);
                var NodeList = new Array(count);
                
                for(let i=0; i<count; i++){
                    let data = await this.contractInstance.getNodeInfo(i);
                    console.log(data);
                    let obj = {phy_address: data[0], address: data[1], reputation: data[2]};
                    NodeList[i] = obj;              
                }
                return NodeList;
            });
        }

        getAuctionInfo(){
            if(this.contractInstance == null){
                console.log("Cannot find contract instance");
                return "Empty";
            }

            return this.contractInstance.getAuctionInfo();
        }


        getDealerPubKey(){
            if(this.contractInstance == null){
                console.log("Cannot find contract instance");
                return "Empty";
            }

            return this.contractInstance.getDealerPubKey();            
        }

        setDealerPubKey(pubkey){
            if(this.contractInstance == null){
                console.log("Cannot find contract instance");
                return false;
            }
            console.log("Publishing pub key");  
            console.log(this.contractInstance.contract);

            this.contractInstance.contract._eth.defaultAccount = this.contractInstance.contract._eth.coinbase
            this.contractInstance.setDealerPubKey(pubkey, {gas: 1500000}).then((result) => {
                console.log(result);
                return true;
            }).catch((err) => {
                console.log(err);
                return false;
            })
        }



}

export default AuctionContract;

