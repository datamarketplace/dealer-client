import React, {Component} from 'react';
import { withStyles } from '@material-ui/core/styles';
import MenuItem from '@material-ui/core/MenuItem';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Button from '@material-ui/core/Button'

import PropTypes from 'prop-types'; // ES6
import NodeComunication from './NodeCommunication'

const styles = theme => ({
    container: {
      display: 'flex',
      flexWrap: 'wrap',
    },
    textField: {
      marginLeft: theme.spacing.unit,
      marginRight: theme.spacing.unit,
      width: 200,
    },
    dense: {
      marginTop: 19,
    },
    menu: {
      width: 200,
    },
  });

class TokenManager extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            privateKey: "",
            generated: true,
            token: ""
        }

        this.handlePrivKeyChange = this.handlePrivKeyChange.bind(this);
        this.generateToken = this.generateToken.bind(this);
        this.handleTokenChange = this.handleTokenChange.bind(this);
        this.insertToken = this.insertToken.bind(this);
        this.handleNewToken = this.handleNewToken.bind(this);
    }


    generateToken(){
        let node_connection = this.props.nodes_list[0];

        console.log(node_connection);

        let conn = node_connection;
        console.log("Generating token");
        conn.generateToken(this.state.privateKey).then( (res) =>{
            console.log("Generated token: " + res.data);

            let _token = res.data;
        
            this.setState({
                token: _token,
                generated: true
            })

        }).catch( (err) => {
            console.log("Error:" + err);
        });

    }


    handlePrivKeyChange(event){
        this.setState({privateKey: event.target.value});
    }

    handleTokenChange(event){
        this.setState({token: event.target.value});
    }

    handleNewToken(){
        this.setState({generated: false});
    }

    insertToken(){
        //Parent callback
        this.props.tokenCallback(this.state.token);
    }


    render(){

        if(this.state.generated){
            return(
                <div>
                    <Typography variant="title">Insert the JWT Token</Typography>
                    <TextField
                    id="txt_token"
                    label="Token"
                    className={styles.textField}
                    type="password"
                    autoComplete="current-password"
                    margin="normal"
                    onChange={this.handleTokenChange}
                    value={this.state.token}
                    />
                    <Button onClick={this.insertToken}>Confirm</Button>
                    <Button onClick={this.handleNewToken}>Generate new token</Button>
                </div>
            );
        }else{
            return(
                <div>
                    <Typography variant="title">Token generation</Typography>
                    <Typography>Insert your private key in order to generate the token</Typography>
                    <TextField 
                        id="txt_priv_key"
                        className={styles.textField}
                        label="Private Key"
                        margin="normal"
                        value={this.state.privateKey}
                        onChange={this.handlePrivKeyChange}
                    />
                    <Button onClick={this.generateToken}>Generate</Button>
                </div>
            );
        }
    }

}
/*

TokenManager.propTypes = {
    tokenCallback: PropTypes.func.isRequired,
    nodes_list: PropTypes.array.isRequired,
}
*/


export default TokenManager;