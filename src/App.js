import React, { Component } from 'react'
//import ProtocolContract from '../build/contracts/Protocol.json'
import DataMarketplaceContract from '../build/contracts/DataMarketplace.json'
import AuctionSelection from './AuctionSelection'

import CssBaseline from '@material-ui/core/CssBaseline';
import Paper from '@material-ui/core/Paper';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';

import styles from './css/app.css'


class App extends Component {
  constructor(props) {
    super(props);

    this.state = {
      web3: null
    }

  }

  render() {
    console.log(styles.container)
    return (
      <div className={styles.root}>
        <CssBaseline /> 
          <div>
            <div className={styles.appbar}>
              <AppBar position="static" color="default">
                <Toolbar>
                  <Typography variant="title" color="inherit">
                    DataMarketplace System
                  </Typography>
                </Toolbar>
              </AppBar>
            </div>

            <main style={{marginLeft: '12%', marginRight: '12%', marginTop: '10%', flexGrow: 1}}>
              <Grid container spacing={16}   
                direction="column"
                justify="center"
                alignItems="center">
                
                <Grid item xs sm={12}>
                  <Typography variant="display1" component="h3">Welcome</Typography>
                    <Typography component="p">Here should go the Datamarketplace contract data</Typography>
                    <Typography component="p">Once selected the auction, the protocol container should handle it</Typography>
                </Grid>
                <Grid item xs sm={12}>
                  <Paper elevation={2} rounded style={{paddingTop: '2%', paddingBottom: '2%', paddingLeft: '5%', paddingRight: '5%'}}>
                    <AuctionSelection></AuctionSelection>
                  </Paper>
                </Grid>
              </Grid>
            </main>
          </div>
        
      </div>
    );
  }
}

export default App
