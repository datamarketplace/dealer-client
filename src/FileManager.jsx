import React, {Component} from 'react';
import { sha3_512 } from 'js-sha3'; //Use directly crypto-js
import FileUpload from './FileUpload'
import { json } from 'graphlib';
import blob from 'blob-util'
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';


class FileManager extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            file_uploaded: false,
            files: null,
            file_hash: null,
            file_pass: "",
            enc_file: null,
            ipfs_ip: "",
            ipfs_port: ""
        };
        this.handleFileUpload = this.handleFileUpload.bind(this);
        this.changeFile = this.changeFile.bind(this);
        this.publishToIPFS = this.publishToIPFS.bind(this);
        this.handleIPChange = this.handleIPChange.bind(this);
    }

    handleFileUpload(files){
        console.log(files);
        this.setState({file_uploaded: true,
                        files: files[0]});  //Just one file support
        console.log("Handle files:" + this.state.files);
        
        let hash = sha3_512(files);
        this.setState({file_hash: hash});
        console.log("Hash Sha3: " + hash);
        if(this.state.file_pass != ""){    //If the password is already set
            console.log("Start encrypting the files...")
            this.encryptFile();             //start encrypt the files   
        }
        this.props.onUpload(hash);
    };

    handleIPChange(event){
        this.setState({ipfs_ip: event.target.value});
    }


    changeFile(){
        this.setState({files: null,
                    file_hash: null,
                    file_uploaded: false});
    }


    publishToIPFS(){
        
    }

    encryptFile(){
        var AES = require("crypto-js/aes");
        //var json_file = JSON.stringify(this.state.files);
        console.log(this.state.files)
        var enc_file = AES.encrypt(this.state.files, "this.state.file_pass");
        console.log("LOL"); 
        console.log(enc_file)
    }

    setFilePassword(_password){
        this.setState({file_pass: _password})
        if(this.state.file_uploaded){
            this.encryptFile();
        }
    }

    render(){
        if(!this.state.file_uploaded){
            return(
                <div>
                    <FileUpload onUpload={this.handleFileUpload}></FileUpload>
                </div>

            );
        }else{
            return(
                <div>
                    <div>
                        <Typography component="h2">File Uploaded</Typography>
                        <Typography component="p">File Name: {this.state.files.name}</Typography>
                        <Typography component="p">Size: {this.state.files.size}</Typography>
                        <Typography component="p">Type: {this.state.files.type}</Typography>
                        <Typography component="p">Hash: {this.state.file_hash}</Typography>
                        <Button onClick={this.changeFile} color="default">Change File</Button>
                    </div>

                    <div id="ipfs_setting">
                        <Typography component="h2">IPFS Settings</Typography>
                        <Typography component="h3">Select IPFS node</Typography>
                        <form>
                            <InputLabel htmlFor="txt_ip">Node Address</InputLabel>
                            <TextField 
                            id="txt_ip"
                            label="IP Address"
                            margin="normal"
                            value={this.state.ipfs_ip}
                            onChange={this.handleIPChange}
                            />
                            <InputLabel htmlFor="txt_port">Node Port</InputLabel>
                            <input type="number" id="txt_port" defaultValue="5001"></input>
                            <Button onClick={this.publishToIPFS} color="primary">Publish</Button>
                        </form>
                    </div>

                </div>
            );
        }
    }

}

export default FileManager