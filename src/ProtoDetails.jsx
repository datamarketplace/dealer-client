import React, {Component} from 'react'
import PropTypes from 'prop-types'; 
import rsa from 'node-rsa';
import Dropdown from 'react-dropdown'
import 'react-dropdown/style.css'
import CircularProgress from '@material-ui/core/CircularProgress';
import purple from '@material-ui/core/colors/purple';
import Select from '@material-ui/core/Select';
import MenuItem from '@material-ui/core/MenuItem';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import InputLabel from '@material-ui/core/InputLabel';
import TextField from '@material-ui/core/TextField';

const styles = theme => ({
    progress: {
      margin: theme.spacing.unit * 2,
    },
  });

var base64 = require('base-64');

/**
 * This component provide input to insert protocol related info, like public keys
 */
class ProtocolSettings extends React.Component{

    constructor(props){
        super(props);

        const _key_size_options = [
            512, 1024, 2048
        ]

        this.state = {
            pubkey: "",
            submitted: false, 
            privkey: "",    //TODO clean memory after the publish
            keysize: 1024,
            key_size_options: _key_size_options,
            generated: false,
            genProgress: false
        };
    
        this.handlePubChange = this.handlePubChange.bind(this);

        this.onKeySizeSelect = this.onKeySizeSelect.bind(this);
        this.generateKeys = this.generateKeys.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
    }


    handlePubChange(event){
        this.setState({pubkey: event.target.value});
    }


    asyncGenKey(keysize){
        return new Promise(function(resolve, reject) {
            console.log(keysize);
            const key = new rsa({b: keysize});            
            console.log("Generated");
            resolve(key);
        });
    }


    generateKeys(event){
        event.preventDefault(); 

        this.setState({genProgress: true});

        this.asyncGenKey(this.state.keysize).then( (res) => {
            console.log("Generated Key: " + res);
            let key = res;
            let _priv_key = key.exportKey('private');
            let _pub_key = key.exportKey('public');
    
            let end_priv_key = base64.encode(_priv_key);

            this.setState({
                privkey: end_priv_key,
                pubkey: _pub_key,
                generated: true,
                genProgress: false
            })
        });

        console.log("Keys generated");
    }

    onKeySizeSelect(event){
        this.setState({keysize: event.target.value});
        console.log("Key size setted to " + event.target.value)
    }

    handleSubmit(event){

        event.preventDefault();

        this.setState({submitted: true});
        console.log("Submitted");

        let encoded_pubkey = base64.encode(this.state.pubkey);  //Always encode PublicKey
        this.props.onSubmit(encoded_pubkey)

    }

    render(){


        if(!this.state.submitted){
            return(
                <div>
                    <Typography component="h2">Protocol Signature</Typography>
                    <Grid container spacing={8}>
                        <Grid item>
                            <InputLabel>
                                Dealer Public Key
                            </InputLabel>
                            <TextField multiline={true}  id="public_key" value={this.state.pubkey} onChange={this.handlePubChange}></TextField>
                        </Grid>
                        {(this.state.pubkey.length ==  0 || this.state.generated) &&
                            <Grid item>
                                <InputLabel>
                                    Dealer Private Key
                                </InputLabel>
                                <TextField multiline={true} id="private_key" value={this.state.privkey} readOnly></TextField>
                            </Grid>
                        }

                        {(this.state.pubkey.length ==  0 || this.state.generated) &&
                            <Grid item>
                                
                                <InputLabel>Key Size</InputLabel>
                                <Select options={this.state.key_size_options} onChange={this.onKeySizeSelect} value={this.state.keysize} placeholder="Select an option" >
                                    <MenuItem value={512}>512</MenuItem>
                                    <MenuItem value={1024}>1024</MenuItem>
                                    <MenuItem value={2048}>2048</MenuItem>
                                </Select>

                                <Button onClick={this.generateKeys}>Generate</Button>
                            </Grid>
                        }
                        {(this.state.genProgress) &&
                            <span>
                                <CircularProgress className={styles.progress} size={50} />
                            </span>
                        }
                        <Grid item>
                            <Typography variant="body1">
                                Publish Public keys on the auction contract
                            </Typography>
                            <Button onClick={this.handleSubmit} color="primary">Publish</Button>
                        </Grid>
                    </Grid>
                </div>
            );
        }else{
            return(
                <div>
                    <p>PubKey: {this.state.pubkey}</p>
                    <p>Key: inserted</p>
                </div>
            );
        }
        
    }
}



ProtocolSettings.PropTypes = {
    requiredFunc: PropTypes.func.isRequired //Callback function
}




export default ProtocolSettings