import React, {Component} from 'react'
import TextField from '@material-ui/core/TextField';
import Button from '@material-ui/core/Button';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';



class KeyForm extends React.Component{
    
    constructor(props){
        super(props);

        this.state = {
            password: "",
            submitted: false
        }

        this.handleKeyChange = this.handleKeyChange.bind(this);
        this.handlePassReset = this.handlePassReset.bind(this);
        this.handleConfirm = this.handleConfirm.bind(this);
    }

    handleKeyChange(event){
        this.setState({password: event.target.value});
        console.log(event.target.value)
    }

    handlePassReset(){
        this.setState({submitted: false,
                    password: ""});
        this.props.onConfirm(null);
    }

    handleConfirm(){

        //TODO password security checks
        this.setState({submitted: true})
        this.props.onConfirm(this.state.password);
    }

    render(){
        console.log("Submitted " + this.state.submitted)
        if(this.state.submitted){
            return(
                <div>
                    <Button component="span" color="secondary" onClick={this.handlePassReset}>Reset Password</Button>
                </div>
            );
        }else{
            return(
                <div>
                    <Typography variant="caption">Password used to encrypt file</Typography>
                    <TextField
                    id="standard-password-input"
                    label="Password"
                    type="password"
                    margin="normal"
                    value={this.state.password}
                    onChange={this.handleKeyChange}
                    />
                    <span>
                        <Button component="span" color="primary" onClick={this.handleConfirm}>Confirm Password</Button>
                    </span>
                </div>
            );
        }
    }

}

export default KeyForm