const NodeRSA = require('node-rsa');
import axios from 'axios';
/**
 * This class provides an interface for cominicating with the node of the off-chain network. 
 * For more information on the protocol see : (XXX)
 */
class NodeCommunication{


    constructor(_phy_address, _port, _pub_key, _auction_address){
        this.node_url = _phy_address;
        this.port = _port;
        this.pub_key = _pub_key;
        this.auction_address = _auction_address;

        this.url = "http://" + this.node_url //+ ":" + this.port
    }

    getNodeURL(){
        return this.node_url;
    }

    isOnline(){
        var url = this.url;
        return new Promise(function(resolve, reject) {
            console.log("Checking URL: " + url + "/status" )
            axios.get(url + "/status").then( (res) =>{
                console.log(res)
                if(res.status === 200)
                    resolve(true);
                reject("Error : " + res);
            }).catch( (err) =>{
                console.log(err);
                reject(err);
            });            
        });

    }

    uploadShare(share, token){
        var auctionAddress = this.auction_address;
        var url = this.url;
        // Make a request for a user with a given ID
        return new Promise(function(resolve, reject) {
            console.log("Start upoloading");
            console.log("Provided Token: " + token)
            console.log("Auction Address: "+ auctionAddress)
            axios.post(url + '/uploadShare/' + auctionAddress, {
                data: share,
                token: token
            }).then(function (response) {
                // handle success
                if(response.data === "ID already present"){
                    reject("id present");
                }
                resolve("ok");
                console.log(response);
            })
            .catch(function (error) {
                // handle error
                console.log("There was an error. " + error);
                reject(error)
            })
            .then(function () {
                console.log("Waiting...")
            });
        });
    }


    generateToken(privateKey){
        var auctionAddress = this.auction_address;
        var url = this.url;
        var node_url = this.node_url;
        return new Promise( function(resolve, reject){
            console.log("Generating token for " + auctionAddress);
            console.log("Start upoloading");
    
            axios.post(url + '/getJWT/' + auctionAddress, {
                priv_key: privateKey,
            }).then(function (response) {
                // handle success
                console.log("Token generated from " + node_url);
                console.log(response);
                resolve(response);
            })
            .catch(function (error) {
                // handle error
                console.log("There was an error. " + error);
                reject(error);
            })
            .then(function () {
               console.log("Waiting...")
            });
        });
    }


}

export default NodeCommunication