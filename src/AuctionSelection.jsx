import React, {Component} from 'react'
import Protocol from './Protocol';
import Button from '@material-ui/core/Button';
import TextField from '@material-ui/core/TextField';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';

class AuctionSelection extends React.Component{

    constructor(props){
        super(props);

        this.state = {
            contract_address: "",
            selected: false,
            custom_address: true        //Not needed
        }

        this.handleAddrChange= this.handleAddrChange.bind(this);
        this.handleDeploy = this.handleDeploy.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);

    }

    handleAddrChange(event){
        this.setState({contract_address: event.target.value})
        console.log(event.target.value)
    }

    handleSubmit(){
        this.setState({custom_address: true,
                    selected: true})
    }

    handleDeploy(){
        this.setState({selected: true,
                custom_address: false});
    }

    render(){
        if(!this.state.selected){
            return(
            <Grid container spacing={8}
                direction="column"
                justify="center"
                alignItems="center"
                alignContent="center"
                style={{paddingLeft: '20px', paddingRight: '20px'}}
            >
                <Grid item>
                    <Typography variant="headline" component="h1" style={{paddingTop: '5px'}}>
                        Choose Contract
                    </Typography>
                </Grid>
                <Grid item>
                    <TextField
                        id="txtContractAddress"
                        label="Contract Address"
                        placeholder="Ex. 0x228D2918..."
                        margin="normal"
                        variant="outlined"
                        onChange={this.handleAddrChange}
                        style={{}}
                    />
                </Grid>
                <Grid item>
                    <Grid container spacing={8} direction="row" justify="center">
                        <Grid item>
                            <Button variant="raised" color="primary" onClick={this.handleSubmit}>Use this</Button>
                        </Grid>
                        <Grid item>
                            <Button variant="raised" color="secondary" onClick={this.handleDeploy}>Deploy</Button>
                        </Grid>

                    </Grid>
                </Grid>
            </Grid>
            );
        }else{

            if(this.state.custom_address){
                return(
                    <Grid container spacing={8}
                        direction="column"
                        justify="center"
                        alignItems="center"
                        alignContent="center"
                    >
                        <Grid item>
                            <Protocol address={this.state.contract_address}></Protocol>
                        </Grid>
                    </Grid>
                );
            }else{
                return(
                    <Grid container spacing={8}
                        direction="column"
                        justify="center"
                        alignItems="center"
                        alignContent="center"
                    >
                        <Grid item>
                            <Protocol address={""}></Protocol>
                        </Grid>
                    </Grid>
                );
            }



        }
    }


}

export default AuctionSelection;