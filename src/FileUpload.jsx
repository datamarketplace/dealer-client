import React, {Component} from 'react';
import Dropzone from 'react-dropzone'

class FileUpload extends React.Component{

    constructor(props) {
        super(props);

        this.state = { files: null };
    }

    componentWillUnmount(){
        console.log("File Upload component unmounted");
    }
      
    onDrop(files) {
        console.log("Files uploaded");
        console.log(files);
        
        //Callback function to parent
        this.props.onUpload(files);
    }



    render(){
        return(
            <div>
                <Dropzone onDrop={(files) => this.onDrop(files)}>
                <div>Try dropping some files here, or click to select files to upload.</div>
                </Dropzone>
            </div>
        );
    }
}

export default FileUpload