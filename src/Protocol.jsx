import React, {Component} from 'react';
import FileManager from './FileManager'
import AuctionContract from './AuctionContract';
import ProtocolSettings from './ProtoDetails';  
import NodeCommunication from './NodeCommunication';
import ShareManager from './ShareManager';
import PasswordForm from './PasswordForm';
import TokenManager from './TokenManager';
import NodeListElem from './NodeListElem';
var base64 = require('base-64');

import Button from '@material-ui/core/Button';
import ExpansionPanel from '@material-ui/core/ExpansionPanel';
import ExpansionPanelSummary from '@material-ui/core/ExpansionPanelSummary';
import ExpansionPanelDetails from '@material-ui/core/ExpansionPanelDetails';
import Typography from '@material-ui/core/Typography';
import Grid from '@material-ui/core/Grid';
import TextField from '@material-ui/core/TextField';
import List from '@material-ui/core/List';
import ListItem from '@material-ui/core/ListItem';
import ListItemIcon from '@material-ui/core/ListItemIcon';
import ListItemText from '@material-ui/core/ListItemText';
import ComputerIcon from '@material-ui/icons/ComputerTwoTone'

import Snackbar from '@material-ui/core/Snackbar';
import IconButton from '@material-ui/core/IconButton';
import CloseIcon from '@material-ui/icons/Close';


/**
 * Manage all the operation necessary to let the protocol works
 */
class Protocol extends React.Component{
    
    constructor(props){
        super(props);

        this.state = {
            auctionName: "",    
            fileDescription: "",
            dealerAddress: "",
            winnerAddress: "",
            participantCount: 0,
            shares: null,
            nodes: new Array(),
            nodes_num: 0,
            dealer_pubkey: "",
            signed_message: "",
            password: "",
            key_published: false,
            pass_confirmed: false,
            file_uploaded: false,
            file_hash: "",
            key: null,
            token: "",
            offchainAvailable: false,    //Describe if there is at least one node online
            render_node: false,
            nodes_html: "",
            showSnackbar: false
        };

        this.contract = null;
        this.listNodes = null;
        this.comunications = null;  //Array of NodeComunications

        this.updateData = this.updateData.bind(this);
        this.setPubKey = this.setPubKey.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.handleKeyChange = this.handleKeyChange.bind(this);
        this.handleShares = this.handleShares.bind(this);
        this.setFileHash = this.setFileHash.bind(this);
        this.handlePasswordConfirm = this.handlePasswordConfirm.bind(this);
        this.createNodeCommunication = this.createNodeCommunication.bind(this);
        this.setToken = this.setToken.bind(this)

    }

    /**
     * Instantiate the auction cotnract and fetch the data
     */
    componentWillMount() {
        if(this.props.address === "")
            this.contract = new AuctionContract(this.updateData);
        else
            this.contract = new AuctionContract(this.updateData, this.props.address)
    }

    /**
     * Fetch Auction info and update the state
     * Info: Winner Address, Dealer Address, Part. Count, Auct. Name, File desc
     */
    updateData() {
        //console.log("Updating Auction info");
        this.contract.getAuctionInfo().then((result) =>{
            console.log("Auction Info : " + result);
            this.setState({auctionName: result[3],
            fileDescription: result[4],
            winnerAddress: result[0],
            dealerAddress: result[1]});  
        });

        //console.log("Getting nodes info");
        this.contract.getNodes().then((result) => {
            console.log("Nodes list fetched");
            this.setState({nodes: result});
            console.log("Creating comunication with nodes");
            this.createNodeCommunication();
        });
        
        //console.log("Getting Dealer PubKey");
        this.contract.getDealerPubKey().then((result) => {
            if(result === ""){
                console.log("No dealer pubkey present");
                result = "";
            }else{
                console.log("Dealer's public key fetched")
                this.setState({key_published: true});
            }
                
            this.setState({dealer_pubkey: result});
        })
    }

    setPubKey(pubkey){
        console.log("Keys: " + base64.decode(pubkey));

        if(this.contract.setDealerPubKey(pubkey)){
            this.setState({dealer_pubkey: pubkey,
                key_published: true});
        }
    }

    createNodeCommunication(){
        console.log("Nodes list:");
        console.log(this.state.nodes)

        this.state.nodes.forEach(node => {
            console.log(node.phy_address);
            this.listNodes += (        
            <ListItem button>
                <ListItemIcon>
                <ComputerIcon />
                </ListItemIcon>
                <ListItemText primary={node.phy_address} />
                <span>lol</span>
            </ListItem>
            );
        });
        this.forceUpdate();

        this.comunications = new Array();

        this.state.nodes.forEach(node => {
            console.log(node);
            let node_comm = new NodeCommunication(node.phy_address, 3000, this.state.pubkey, node.address);
            console.log("Cheking if node " + node_comm.getNodeURL() + " is active");
            node_comm.isOnline().then( (res) => {
                console.log(res);
                if(res){
                    console.log("Node is online");
                    this.comunications.push(node_comm);  //Push into the comunication array
                    this.setState({offchainAvailable: true})
                }else{
                    console.log("Unable to reach " + node);
                }
            });
        });
    }

    handleSubmit(){
        //TODO Check if all the data were inserted

        this.comunications.forEach(node => {
            console.log("Cheking if node " + node.getNodeURL() + " is active");
            node.isOnline().then( (res) => {
                console.log(res);
                if(res){
                    console.log("Node is online");
                    console.log(this.state.shares[0]);
                    node.uploadShare(this.state.shares[0], this.state.token ,this.state.auctionName).then( (res) => {
                        this.setState({showSnackbar: true})
                    }).catch( (err) => {
                        console.log("Error: " + err)
                    });
                }else{
                    console.log("Node " + node.getNodeURL() + " is offline");
                }
            }).catch( (err) => {
                console.log(err);
                console.log("Node " + node.getNodeURL() + " is offline");

            });
        })

    }

    handleKeyChange(){
        this.setState({key_published: false});
    }

    handleShares(_shares){
        console.log("Shares created");
        console.log(_shares);
        this.setState({shares: _shares});
    }

    setFileHash(hash){
        console.log("File Hash: " + hash);
        this.setState({file_hash: hash});
    }


    handlePasswordConfirm(_password){
        console.log("Definitive password : " + _password);
        if(_password === null){ //Password reset
            this.setState({password: "",
                pass_confirmed: false})
        }else{
            this.setState({password: _password,
                pass_confirmed: true})

            this.FileManager.setFilePassword(_password);
        }
    }

    setToken(_token){
        this.setState({token: _token})
        console.log("Token set to: " + _token)
    }

    handleClick = () => {
        this.setState({ showSnackbar: true });
    };
    
    handleClose = (event, reason) => {
        if (reason === 'clickaway') {
          return;
        }

        this.setState({ showSnackbar: false });
    };

    render(){


        var protocol_setting;

        if(!this.state.key_published){
            protocol_setting = <ProtocolSettings onSubmit={this.setPubKey}></ProtocolSettings>
        }
        else
            protocol_setting = <div>Keys Published correctly<Button onClick={this.handleKeyChange}>Change Keys</Button></div>

        return(
            <div style={{margin: '3%'}}>
                <ExpansionPanel id="auction_launch">
                    <ExpansionPanelSummary>
                        <Typography variant="display1" gutterBottom>File Upload</Typography>
                    </ExpansionPanelSummary>
                    <ExpansionPanelDetails>
                        <Grid container justify="center" alignItems="center" direction="column">
                            <Grid item>
                                <Typography variant="subheading" gutterBottom>
                                    Function related to the file to sell.
                                </Typography>
                            </Grid>
                            <Grid item>
                                <FileManager oPasswordFormd={this.setFileHash} ref={instance => { this.FileManager = instance; }}/>
                            </Grid>
                        </Grid>
                    </ExpansionPanelDetails>
                </ExpansionPanel>

                <div id="auction_details">
                    <ExpansionPanel id="auction_details">
                        <ExpansionPanelSummary>
                            <Typography variant="display1" gutterBottom>Auction Details</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <Grid container spacing={8} justify="center" alignItems="center" alignContent="center" direction="row">
                                <Grid item>
                                    <Typography variant="headline">{this.state.auctionName}</Typography>
                                </Grid>
                                <Grid item>
                                    <Typography variant="caption">{this.state.fileDescription}</Typography>
                                </Grid>
                                <Grid item>
                                    <Typography variant="body1">Winner Address: {this.state.winnerAddress}.</Typography>
                                </Grid>
                                <Grid item>
                                    <Typography noWrap variant="body1">Participant Number: {this.state.participantCount}</Typography>
                                </Grid>
                                <Grid item xs>
                                    <TextField value={base64.decode(this.state.dealer_pubkey)} label="Dealer Public Key" multiline readOnly></TextField>                            
                                </Grid>
                            </Grid>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                </div>
                <div id="node_details">
                    <ExpansionPanel>
                        <ExpansionPanelSummary>
                            <Typography variant="display1">Nodes Details</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <List component="nav">
                                {this.state.nodes.map(node => (
                                    <NodeListElem node={node}></NodeListElem>
                                ))}
                            </List>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>

                </div>
                <div id="proto_settings">
                    <ExpansionPanel>
                        <ExpansionPanelSummary>
                            <Typography variant="display1" gutterBottom>Protocol Settings</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            {protocol_setting}  
                        </ExpansionPanelDetails>
                    </ExpansionPanel>                
                </div>
                <div id="pass_form">
                    <ExpansionPanel>
                        <ExpansionPanelSummary>
                            <Typography variant="display1" gutterBottom>Password Form</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <PasswordForm onConfirm={this.handlePasswordConfirm}></PasswordForm>                        
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                </div>
                <div id="shares_manager">
                    <ExpansionPanel disabled={!this.state.pass_confirmed}>
                        <ExpansionPanelSummary>
                            <Typography variant="display1" gutterBottom>Shares Manager</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            {this.state.pass_confirmed && 
                            <ShareManager onComplete={this.handleShares}/>
                            }
                        </ExpansionPanelDetails>
                    </ExpansionPanel>

                </div>
                <div id="token_manager">
                    <ExpansionPanel disabled={!this.state.offchainAvailable}>       
                        <ExpansionPanelSummary>
                            <Typography variant="display1">Token Manager</Typography>
                        </ExpansionPanelSummary>
                        <ExpansionPanelDetails>
                            <TokenManager tokenCallback={this.setToken} nodes_list={this.comunications}></TokenManager>
                        </ExpansionPanelDetails>
                    </ExpansionPanel>
                </div>
                <p><Button color="default" onClick={this.handleSubmit}>Invia ai Nodi</Button></p>

                <Snackbar
                anchorOrigin={{
                    vertical: 'bottom',
                    horizontal: 'left',
                }}
                open={this.state.showSnackbar}
                autoHideDuration={6000}
                onClose={this.handleClose}
                ContentProps={{
                    'aria-describedby': 'message-id',
                }}
                message={<span id="message-id">File Uploaded</span>}
                action={[
                    <Button key="undo" color="secondary" size="small" onClick={this.handleClose}>
                    CLOSE
                    </Button>,
                    <IconButton
                    key="close"
                    aria-label="Close"
                    onClick={this.handleClose}
                    >
                    <CloseIcon />
                    </IconButton>,
                ]}
                />

            </div>
        );
    }

}

export default Protocol